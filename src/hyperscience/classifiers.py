from sklearn.feature_extraction.text import TfidfVectorizer
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from gensim.models.wrappers import FastText


def build_vector(model, tokenized_text, idf_scores, stopwords):
    vectors = []
    weights = []
    for token in tokenized_text:
        if token in stopwords:
            continue
        try:
            vectors.append(list(model[token]))
            weights.append(idf_scores.get(token, 1.0))
        except:
            continue
    if not vectors:
        return np.zeros(100)
    repres = np.average(vectors, weights=np.array(weights) / sum(weights), axis=0)
    return repres


class TfIdfLogreg:
    def __init__(self, unsup, model):
        with open('../stopwords.txt', 'r') as f:
            self.stopwords = set([x.strip() for x in f])
        self.tfidf = TfidfVectorizer(min_df=20, ngram_range=(1, 2), stop_words=self.stopwords)
        raw_documents = [a['title'] + a['text'] for a in unsup]
        self.tfidf.fit(raw_documents)
        self.idf = dict(zip(self.tfidf.get_feature_names(), self.tfidf.idf_))
        self.model = model

    def fit(self, train, unsup):
        train_raw_documents = [a['title'] + a['text'] for a in train]
        train_data = self.tfidf.transform(train_raw_documents)

        self.logreg = LogisticRegression(penalty='l2')
        labels = [a['fake_news'] for a in train]

        self.logreg.fit(train_data, labels)
        self.most_important = np.argsort(np.abs(self.logreg.coef_[0, :]))[::-1][:4000]

        train_vector_titles = np.array(
            [list(build_vector(self.model, a['tokenized_title'], self.idf, self.stopwords))
             for a in train])
        train_vector_text = np.array(
            [list(build_vector(self.model, a['tokenized_text'][:200], self.idf, self.stopwords))
             for a in train])

        train_data = train_data[:, self.most_important]

        train_data = np.hstack((train_data.todense(), train_vector_titles, train_vector_text))

        self.rf = RandomForestClassifier(100)
        self.rf.fit(train_data, labels)

    def predict(self, test):
        test_raw_documents = [a['title'] + a['text'] for a in test]

        test_data = self.tfidf.transform(test_raw_documents)

        test_data = test_data[:, self.most_important]

        test_vector_titles = np.array(
            [list(build_vector(self.model, a['tokenized_title'], self.idf, self.stopwords))
             for a in test])
        test_vector_text = np.array(
            [list(build_vector(self.model, a['tokenized_text'], self.idf, self.stopwords))
             for a in test])

        test_data = np.hstack((test_data.todense(), test_vector_titles, test_vector_text))


        return self.rf.predict(test_data), self.rf.predict_proba(test_data)
