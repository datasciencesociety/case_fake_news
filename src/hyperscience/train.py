import tensorflow as tf
import argparse
import glob
import os
import utils
import numpy as np
import pickle
import tensorflow.contrib.metrics as M
import fasttext


def parse_datapoint(example):
    feature_map = {
        'labels': tf.VarLenFeature(dtype=tf.int64),
        'length': tf.FixedLenFeature([], dtype=tf.int64, default_value=-1),
        'target': tf.FixedLenFeature([], dtype=tf.int64, default_value=-1),
        'url': tf.VarLenFeature(dtype=tf.string),
        'title': tf.VarLenFeature(dtype=tf.int64)
    }

    features = tf.parse_single_example(example, feature_map)

    labels = tf.sparse_tensor_to_dense(features['labels'])

    return {'label': labels,
            'target': features['target'],
            'length': features['length'],
            'url': features['url']}


def forward(data_dir, model, is_training, batch_size, dictionary, labels_mapping):
    filename_queue = tf.train.string_input_producer(
        glob.glob(data_dir + '*'), shuffle=is_training, num_epochs=None)

    options = tf.python_io.TFRecordOptions(
        compression_type=tf.python_io.TFRecordCompressionType.ZLIB)

    reader = tf.TFRecordReader(options=options)
    _, serialized_example = reader.read(filename_queue)

    serialized_example = tf.train.shuffle_batch(
        [serialized_example],
        batch_size=1,
        capacity=3000,
        num_threads=8,
        allow_smaller_final_batch=True,
        min_after_dequeue=50)

    serialized_example = tf.squeeze(serialized_example, axis=0)

    example = parse_datapoint(serialized_example)

    labels, targets, lengths, urls = tf.train.batch(
        [example['label'], example['target'], example['length'], example['url']],
        batch_size=batch_size, dynamic_pad=True,
        allow_smaller_final_batch=True)

    with tf.device('cpu:0'):
        word_embedding_var = tf.get_variable(
            "word_embeddings", shape=[len(dictionary), 100], dtype=tf.float32)

        embedding_placeholder = tf.placeholder(tf.float32, [len(dictionary), 100])
        embedding_init = word_embedding_var.assign(embedding_placeholder)

        embedded_labels = tf.nn.embedding_lookup(word_embedding_var, labels)

    feature_map = model.forward(embedded_labels, is_training)
    reduced_feature_map = tf.reduce_mean(feature_map, axis=1)
    logits = tf.layers.dense(reduced_feature_map, len(labels_mapping))

    return {'logits': logits,
            'urls': urls,
            'targets': targets,
            'embedding_init_op': embedding_init,
            'embedding_placeholder': embedding_placeholder}


class ConvolutionalModel:
    def forward(self, labels, is_training):
        h = tf.layers.conv1d(labels, 64, 3, activation=tf.nn.relu, padding="same")
        h = tf.layers.conv1d(labels, 64, 3, activation=tf.nn.relu, padding="same")
        h = tf.layers.dropout(h, training=is_training)
        h = tf.layers.max_pooling1d(h, 2, 2)
        h = tf.layers.conv1d(labels, 128, 3, dilation_rate=2, activation=tf.nn.relu, padding="same")
        h = tf.layers.conv1d(labels, 128, 3, dilation_rate=4, activation=tf.nn.relu, padding="same")
        h = tf.layers.conv1d(labels, 128, 3, activation=tf.nn.relu, padding="same")
        h = tf.layers.dropout(h, training=is_training)
        h = tf.layers.max_pooling1d(h, 2, 2)
        h = tf.layers.conv1d(labels, 256, 3, activation=tf.nn.relu, padding="same")
        h = tf.layers.conv1d(labels, 256, 3, dilation_rate=2, activation=tf.nn.relu, padding="same")
        h = tf.layers.conv1d(labels, 256, 3, dilation_rate=4, activation=tf.nn.relu, padding="same")
        h = tf.layers.dropout(h, training=is_training)
        h = tf.layers.conv1d(labels, 256, 3, activation=tf.nn.relu, padding="same")
        return h

    def error(self, logits, targets):
        loss = tf.nn.sparse_softmax_cross_entropy_with_logits(labels=targets, logits=logits)
        return loss


class ByteNet:
    def encode_layer(self, input_, dilation, layer_no, last_layer = False):
        relu1 = tf.nn.relu(input_, name='enc_relu1_layer{}'.format(layer_no))
        conv1 = tf.layers.conv1d(relu1, 256, 3, name='enc_conv1d_1_layer{}'.format(layer_no), padding="same")
        relu2 = tf.nn.relu(conv1, name='enc_relu2_layer{}'.format(layer_no))
        dilated_conv = tf.layers.conv1d(relu2, 256, 3, dilation_rate=dilation,
                                        name="enc_dilated_conv_layer{}".format(layer_no), padding="same")
        relu3 = tf.nn.relu(dilated_conv, name='enc_relu3_layer{}'.format(layer_no))
        conv2 = tf.layers.conv1d(relu3, 2 * 256, 3, name='enc_conv1d_2_layer{}'.format(layer_no), padding="same")
        return input_ + conv2

    def forward(self, labels, is_training):
        conv1 = tf.layers.conv1d(labels, 512, 3, name='enc_conv1d_0_layer', padding="same")
        current_input  = conv1
        for layer_no, dilation in enumerate([1, 2, 4, 8, 1, 2, 4, 1]):
            layer_output = self.encode_layer(current_input, dilation, layer_no)
            current_input = layer_output

        return current_input

    def error(self, logits, targets):
        loss = tf.nn.sparse_softmax_cross_entropy_with_logits(labels=targets, logits=logits)
        return loss


def get_train_op(loss):
    optimizer = tf.train.AdamOptimizer(1e-3)
    return optimizer.minimize(loss)


def evaluate(data_dir, batch_size, checkpoint, validate_output_dir, max_iters,
             dictionary, labels, pretrained_embeddings=None):
    with tf.Graph().as_default():
        model = ByteNet()
        tensors = forward(data_dir, model, False, batch_size, dictionary, labels)

        probabilities = tf.nn.softmax(tensors['logits'])

        loss_tensor = model.error(tensors['logits'], tensors['targets'])

        predictions = {
            'probabilities': probabilities,
            'predictions': tf.argmax(probabilities, axis=1)}

        metrics = {
            'loss': M.streaming_mean(loss_tensor),
            'accuracy': M.streaming_accuracy(predictions['predictions'], tensors['targets'])
        }

        init_op = tf.global_variables_initializer()
        local_init_op = tf.local_variables_initializer()

        metrics_reset = tf.variables_initializer([
             var for var in tf.local_variables()
             if var.name.startswith("metrics/")])

        metrics_read, metrics_update = M.aggregate_metric_map(metrics)

        summary_op = tf.summary.merge_all()

        session = tf.Session()
        session.run([init_op, local_init_op])

        if pretrained_embeddings is not None:
            session.run(
                tensors['embedding_init_op'],
                feed_dict={tensors['embedding_placeholder']: pretrained_embeddings})

        coord = tf.train.Coordinator()
        tf.train.start_queue_runners(sess=session, coord=coord)

        if checkpoint:
            tf.train.Saver(tf.all_variables()).restore(session, checkpoint)

        for i in range(16):
            fetches = [loss_tensor, metrics_update, summary_op, predictions, tensors['targets'], tensors['urls']]
            try:
                loss, ms, summary, pred_data, gt, urls = session.run(fetches)
            except tf.errors.OutOfRangeError:
                break
        print("Validation metrics:", ms)


def train(data_dir, is_training, batch_size, checkpoint,
          ignore_missing_vars, model_dir, validate_output_dir, max_iters,
          dictionary, labels, pretrained_embeddings=None, validation_dir=None):
    with tf.Graph().as_default():
        model = ByteNet()
        tensors = forward(data_dir, model, is_training, batch_size, dictionary, labels)

        probabilities = tf.nn.softmax(tensors['logits'])

        loss_tensor = model.error(tensors['logits'], tensors['targets'])

        predictions = {
            'probabilities': probabilities,
            'predictions': tf.argmax(probabilities, axis=1)}

        metrics = {
            'loss': M.streaming_mean(loss_tensor),
            'accuracy': M.streaming_accuracy(predictions['predictions'], tensors['targets'])
        }

        train_op = get_train_op(loss_tensor)

        global_step = tf.Variable(0, name='global_step', trainable=False, dtype=tf.int32)

        global_step_tensor = tf.train.get_global_step()

        init_op = tf.global_variables_initializer()
        local_init_op = tf.local_variables_initializer()

        metrics_reset = tf.variables_initializer([
             var for var in tf.local_variables()
             if var.name.startswith("metrics/")])

        metrics_read, metrics_update = M.aggregate_metric_map(metrics)

        summary_op = tf.summary.merge_all()

        session = tf.Session()
        session.run([init_op, local_init_op])

        if pretrained_embeddings is not None:
            session.run(
                tensors['embedding_init_op'],
                feed_dict={tensors['embedding_placeholder']: pretrained_embeddings})

        saver = tf.train.Saver(tf.all_variables(), keep_checkpoint_every_n_hours=1, max_to_keep=30)

        if checkpoint:
            restore_checkpoint(session, checkpoint, ignore_missing_vars)

        summary_writer = tf.summary.FileWriter(model_dir)

        coord = tf.train.Coordinator()
        tf.train.start_queue_runners(sess=session, coord=coord)

        if validate_output_dir:
            os.makedirs(validate_output_dir)

        for i in range(max_iters):
            fetches = [loss_tensor, metrics_update, summary_op, predictions, tensors['targets']]
            if not validate_mode:
                fetches.extend([global_step_tensor, train_op])
            else:
                fetches.extend([tensors['urls']])

            try:
                if not validate_mode:
                    loss, ms, summary, pred_data, gt, step, _, = session.run(fetches)
                else:
                    loss, ms, summary, pred_data, gt, scans, slices = session.run(fetches)

                    predictions_objects = utils.split_batch(
                        scans, slices, pred_data['predictions'], pred_data['probabilities'])

                    for p in predictions_objects:
                        with open(
                                os.path.join(validate_output_dir,
                                             p.scan_id + '.' + p.slice_id), 'wb') as f:
                            pickle.dump(p.predictions.astype(np.uint8), f)

                if (i % 20 == 0):
                    summary_writer.add_summary(summary)
                    print("Step", i, ms)
                    session.run(metrics_reset)

                if i > 0 and i % 500 == 0 and not validate_mode:
                    saver.save(session, os.path.join(model_dir, 'model'), i)
                    if validation_dir:
                        evaluate(validation_dir, batch_size,
                                 os.path.join(model_dir, 'model-{}'.format(i)),
                                 validate_output_dir,
                                 max_iters,
                                 dictionary,
                                 labels,
                                 pretrained_embeddings)

            except tf.errors.OutOfRangeError:
                break

        session.close()


if '__main__' == __name__:
    parser = argparse.ArgumentParser(description='Convert to dataset.')
    parser.add_argument(
        '--data_dir', type=str, required=True,
        help='Train tf records prefix.')
    parser.add_argument(
        '--model_dir', type=str, required=True,
        help='Model directory.')
    parser.add_argument(
        '--embeddings_file', type=str, required=False,
        help='Embeddings file.')
    parser.add_argument(
        '--batch_size', type=int, required=False, default=4,
        help='Batch size.')
    parser.add_argument(
        '--max_iters', type=int, required=False, default=100000,
        help='Max model iterations.')
    parser.add_argument(
        '--checkpoint', type=str, required=False, default=None,
        help='Checkpoint to restore.')
    parser.add_argument(
        '--ignore_missing_vars', action='store_true', required=False, default=None,
        help='Ignore missing vars')
    parser.add_argument(
        '--validate_output_dir', type=str, required=False, default=None,
        help='Validata output directory.')
    parser.add_argument(
        '--validation_data_dir', type=str, required=False, default=None,
        help='Validate data dir')
    parser.add_argument(
        '--dictionary_file', type=str, required=False, default=None,
        help='Word dictionary.')
    parser.add_argument(
        '--labels_file', type=str, required=False, default=None,
        help='Labels file.')

    args = parser.parse_args()

    with open(args.dictionary_file, 'r') as f:
        dictionary = [x.strip() for x in f]

    with open(args.labels_file, 'r') as f:
        labels = [x.strip() for x in f]

    validate_mode = args.validate_output_dir is not None

    num_epochs = 1 if validate_mode else None
    shuffle = (not validate_mode)

    embeddings = None
    if args.embeddings_file:
        embeddings = np.loadtxt(args.embeddings_file)

    train(args.data_dir, not validate_mode, args.batch_size,
          args.checkpoint, args.ignore_missing_vars, args.model_dir,
          args.validate_output_dir, args.max_iters, dictionary, labels, embeddings, args.validation_data_dir)
