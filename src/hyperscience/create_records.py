import tensorflow as tf
import re
import os
import csv
import sys
import argparse
from utils import tokenize, parse_article
import uuid
import itertools


csv.field_size_limit(sys.maxsize)


tf_options = tf.python_io.TFRecordOptions(
    compression_type=tf.python_io.TFRecordCompressionType.ZLIB)


def _bytes_feature(value):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=value))


def _int64_feature(value):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=value))


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]


def grouper(n, iterable):
    it = iter(iterable)
    while True:
       chunk = tuple(itertools.islice(it, n))
       if not chunk:
           return
       yield chunk


WINDOW_SIZE = 30
STEP = 15


def process_rows(rows):
    writer = tf.python_io.TFRecordWriter(
        os.path.join(args.output_directory, 'data-{}.example'.format(uuid.uuid4())), tf_options)
    for row in rows:
        data = parse_article(row)
        data['title_labels'] = [dictionary[x] for x in data['tokenized_title']]

        text = data['tokenized_title'] + data['tokenized_text']
        casing = data['case_text'] + data['case_text']
        for i in range(0, len(text) - WINDOW_SIZE, STEP):
            words = text[i:i + WINDOW_SIZE]
            current_case = casing[i:i + WINDOW_SIZE]
            labels = [dictionary[x] for x in words]

            example = tf.train.Example(features=tf.train.Features(
                feature={'url': _bytes_feature([data['url'].encode()]),
                         'labels': _int64_feature(labels),
                         'case': _int64_feature(current_case),
                         'titles': _int64_feature(data['title_labels']),
                         'pos': _int64_feature([i]),
                         'length': _int64_feature([len(labels)]),
                         'target': _int64_feature([targets[data['domain']]])}))
            writer.write(example.SerializeToString())


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Convert to dataset.')
    parser.add_argument(
        '--input_file', type=str, required=True,
        help='Input file.')
    parser.add_argument(
        '--input_dictionary', type=str, required=True,
        help='Input dictionary file.')
    parser.add_argument(
        '--input_labels_file', type=str, required=True,
        help='Input labels file')
    parser.add_argument(
        '--output_directory', type=str, required=True,
        help='Output directory.')
    args = parser.parse_args()

    os.makedirs(args.output_directory)

    with open(args.input_dictionary, 'r') as f:
        dictionary = dict([(x.strip(), i) for i, x in enumerate(f.readlines())])

    with open(args.input_labels_file, 'r') as f:
        targets = dict([(x.strip(), i) for i, x in enumerate(f.readlines())])

    with open(args.input_file, 'r') as f:
        for rows in grouper(32, csv.DictReader(f, delimiter=',')):
            process_rows(rows)
