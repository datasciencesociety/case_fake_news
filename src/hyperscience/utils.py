import urllib
import urllib.parse
import re

tokenize_pattern = re.compile("[A-Z]{2,}(?![a-z])|[A-Z][a-z]+(?=[A-Z])|[\'\w\-]+")


def tokenize(text):
    tokenized_text = re.findall(tokenize_pattern, text)
    case_title = [x.isupper() for x in tokenized_text]
    return [x.lower() for x in tokenized_text], case_title


def parse_article(row):
    tokenized_title, case_title = tokenize(row['Content Title'])
    tokenized_text, case_text = tokenize(row['Content'])
    url = row['url'] if 'url' in row else row['Content Url']
    domain = urllib.parse.urlparse(url).netloc
    fake_news_score = row.get('fake_news_score', None)
    if not fake_news_score:
        fake_news_score = row.get('click_bait_score')
    date_published = (row.get('Content Published Time') if 'Content Published Time' in row
                      else row['date_published'])
    return {'title': row['Content Title'],
            'text': row['Content'],
            'tokenized_text': tokenized_text,
            'tokenized_title': tokenized_title,
            'case_title': case_title,
            'case_text': case_text,
            'url': url,
            'domain': domain,
            'fake_news': fake_news_score == '3' if fake_news_score else None,
            'date_published': date_published}
