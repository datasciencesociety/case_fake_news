import argparse
from utils import tokenize, parse_article
import csv
from itertools import chain
import sys

csv.field_size_limit(sys.maxsize)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Convert to dataset.')
    parser.add_argument(
        '--input_file', type=str, required=True,
        help='Input file.')
    parser.add_argument(
        '--input_big_file', type=str, required=True,
        help='Input big file.')
    parser.add_argument(
        '--input_embeddings', type=str, required=True,
        help='Input dictionary file.')
    parser.add_argument(
        '--output_file', type=str, required=True,
        help='Output file.')
    parser.add_argument(
        '--output_text', type=str, required=True,
        help='Output text.')
    args = parser.parse_args()
    wordset = set()
    index = []
    with open(args.input_embeddings, 'r') as f:
        for row in f:
            word = row.partition(' ')[0]
            wordset.add(word)
            index.append(word)

    unique_in_articles = set()
    with open(args.input_file, 'r') as f_train:
        with open(args.input_big_file, 'r') as f_big:
            with open(args.output_text, 'w') as f_output:
                for row in chain(csv.DictReader(f_train, delimiter=','),
                                 csv.DictReader(f_big, delimiter=',')):
                    data = parse_article(row)

                    for word in data['tokenized_title'] + data['tokenized_text']:
                        unique_in_articles.add(word)
                        if word not in wordset:
                            wordset.add(word)
                            index.append(word)
                    f_output.write(" ".join(data['tokenized_title']) + "\n")
                    f_output.write(" ".join(data['tokenized_text']) + "\n")

    print("Unique words in articles:", len(unique_in_articles))

    with open(args.output_file, 'w') as f:
        for word in index:
            f.write(word + '\n')
