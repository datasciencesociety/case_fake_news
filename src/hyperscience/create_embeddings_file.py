import argparse
import sys
import csv
import fasttext
import numpy as np


csv.field_size_limit(sys.maxsize)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Create embeddings file.')
    parser.add_argument(
        '--input_dictionary_file', type=str, required=True,
        help='Input file.')
    parser.add_argument(
        '--input_model_path', type=str, required=True,
        help='Path to the model')
    parser.add_argument(
        '--output_file', type=str, required=True,
        help='Output file.')
    args = parser.parse_args()

    model = fasttext.load_model(args.input_model_path)

    embeddings = []
    with open(args.input_dictionary_file, 'r') as f:
        for line in f:
            key = line.strip()
            vector = model[key]
            embeddings.append(vector)
    np.save(args.output_file, embeddings)
