import argparse
from utils import tokenize, parse_article
import csv
from itertools import chain
import sys

csv.field_size_limit(sys.maxsize)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Convert to dataset.')
    parser.add_argument(
        '--input_file', type=str, required=True,
        help='Input file.')
    parser.add_argument(
        '--input_big_file', type=str, required=True,
        help='Input big file.')
    parser.add_argument(
        '--output_file', type=str, required=True,
        help='Output file.')
    args = parser.parse_args()

    classes = set()
    with open(args.input_file, 'r') as f_train:
        with open(args.input_big_file, 'r') as f_big:
            for row in chain(csv.DictReader(f_train, delimiter=','),
                             csv.DictReader(f_big, delimiter=',')):
                data = parse_article(row)
                classes.add(data['domain'])

    with open(args.output_file, 'w') as f:
        for cls in classes:
            f.write(cls + '\n')
